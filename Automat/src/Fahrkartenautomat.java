﻿import java.util.Scanner;
class Fahrkartenautomat{
	
	static String password="admin";
	static geld[] geld = new geld[10];
    public static void main(String[] args) {
    	geldDeklarieren();
		Scanner tastatur = new Scanner(System.in);
    	
    	boolean x=true;
    	while (x) {
    		System.out.println("Enter any Thing to start");
    		tastatur.nextLine();
	    	System.out.println("Fahrkarten bestellen (b)\nAdminstrator (a): ");
	    	String choice1=tastatur.nextLine();
	    	while (!choice1.equals("b")&&!choice1.equals("a")) {
	    		System.out.println("Falsche Eingabe.");
	    		System.out.println("Fahrkarten bestellen (b)\nAdminstrator (a): ");
	        	choice1=tastatur.nextLine();
	    	}
	    	if (choice1.equals("b")) {
	    		boolean check= geldLeftCheck();
	    		if (check) {
	    			ticketBestellung();
	    		}else {
	    			System.out.println("Out Of Order");
	    		}
	    	}else if (choice1.equals("a")) {
	    		System.out.println("Password:");
	    		String pass=tastatur.nextLine();
	    		if(pass.equals(password)) {
	    			System.out.println("Kasseleeren (l)\nKasseausfüllen (f)\nVerbleibendes Geld zeigen (z): ");
	            	choice1=tastatur.nextLine();
	            	while (!choice1.equals("l")&&!choice1.equals("f")&&!choice1.equals("z")) {
	            		System.out.println("Falsche Eingabe.");
	            		System.out.println("Kasseleeren (l)\nKasseausfüllen (f)\nVerbleibendes Geld zeigen (z): ");
	                	choice1=tastatur.nextLine();
	            	}
	            	if (choice1.equals("l")) {
	            		kasseLeeren();
	            		System.out.println("\nDone Boss!");
	            		warte(100,10);
	            	}else if (choice1.equals("f")) {
	            		kasseausfüllen();
	            		System.out.println("\nDone Boss!");
	            		warte(100,10);
	            	}else if (choice1.equals("z")){
	            		geldZeigen();
	            	}
	    		}else {System.out.println("WRONG!!!");}
	    	}
		    }
    	System.out.println("\n\n");
	    }
    public static void geldDeklarieren() {
    	geld schein50 = new geld("50 Euro Schein",20);
    	geld schein20 = new geld("20 Euro Schein",20);
    	geld schein10 = new geld("10 Euro Schein",20);
    	geld schein5 = new geld("5 Euro Schein",20);
    	geld münze200 = new geld("2 Euro Münze",20);
    	geld münze100 = new geld("1 Euro Münze",20);
    	geld münze50 = new geld("50 Cent Münze",20);
    	geld münze20 = new geld("20 Cent Münze",20);
    	geld münze10 = new geld("10 Cent Münze",20);
    	geld münze5 = new geld("5 Cent Münze",20);
    	geld[0]  = schein50;
    	geld[1]  = schein20;
    	geld[2]  = schein10;
    	geld[3]  = schein5;
    	geld[4]  = münze200;
    	geld[5]  = münze100;
    	geld[6]  = münze50;
    	geld[7]  = münze20;
    	geld[8]  = münze10;
    	geld[9]  = münze5;
    }
    
    public static boolean geldLeftCheck() {
    	boolean result=true;
    	for (int i=0;i<geld.length;i++) {
    		if (geld[i].amount<5) {
    			result=false;
    		}
    	}
    	return result;
    }
    public static void adminstrator() {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.println("Enter The Password:");
    	String epassword=tastatur.nextLine();
    	if (epassword.equals(password)) {
    		System.out.println("Kasse leeren (l)\nKasse ausfüllen (a)");
    		String choice2=tastatur.nextLine();
    		while (!choice2.equals("l")||!choice2.equals("a")) {
        		System.out.println("Falsche Eingabe.");
        		System.out.println("Kasse leeren (l)\nKasse ausfüllen (a)");
            	choice2=tastatur.nextLine();
        	}
    		if (choice2.equals("l")) {
    			
    		}else if (choice2.equals("k")) {
    			
    		}
    	}else {
    		System.out.println("WRONG!!!");
    	}
    }
    public static void geldZeigen() {
    	for (int i=0;i<geld.length;i++) {
    		System.out.println("you still have "+geld[i].amount+" of "+geld[i].typ);
    	}
    	warte(50,10);
    }
    public static void kasseLeeren() {
    	int amountLeft=0;
    	Scanner tastatur = new Scanner(System.in);
    	System.out.println("Bitte Eingebe wieviel Sie in die Kasse lassen wollen:");
    	String amountLeft_=tastatur.nextLine();
    	boolean choiceIsInt=isInteger(amountLeft_);
    	int x =1;
	       while (x==1||choiceIsInt==false) {    
	        	while (choiceIsInt==false) {
		     	   System.out.println("\nFlasche Eingabe, Bitte nochmal versuchen.");
		     	   System.out.println("Bitte Eingebe wieviel Sie in die Kasse lassen wollen(Nur Zahlen, Min:0 Max:50): ");
		     	  amountLeft_ = tastatur.nextLine();
		     	 choiceIsInt=isInteger(amountLeft_);
		        }
	        	x=0;
	        	amountLeft = Integer.parseInt(amountLeft_);
		        if (amountLeft >50||amountLeft<0) {
		        	
		        	choiceIsInt=false;
		        }
		   }
	    for (int i=0;i<geld.length;i++) {
	    	if (geld[i].amount>=amountLeft) {
	    		geld[i].amount-=(geld[i].amount-amountLeft);
	    	}
	    	
	    }
	    warte(50,10);
	    
    }
    public static void kasseausfüllen() {
    	int amountToAdd=0;
    	int x=1;
    	Scanner tastatur = new Scanner(System.in);
    	System.out.println("Bitte Eingeben bis wieviel Sie die Kasse Ausfüllen wollen:");
    	String auswahl_=tastatur.nextLine();
    	boolean auswahlIsInt=isInteger(auswahl_);
	    while (x==1||auswahlIsInt==false) {    
        	while (auswahlIsInt==false) {
	     	   System.out.println("\nFlasche Eingabe, Bitte nochmal versuchen.");
	     	   System.out.print("Bitte Eingeben bis wieviel Sie die Kasse Ausfüllen wollen:");
	     	   auswahl_ = tastatur.nextLine();
	     	   auswahlIsInt=isInteger(auswahl_);
	        }
        	x=0;
        	amountToAdd = Integer.parseInt(auswahl_);
	        if (amountToAdd >100||amountToAdd<0) {
	        	x=1;
	        	auswahlIsInt=false;
	        }
	    }
	    for (int i=0;i<geld.length;i++) {
	    	if (geld[i].amount<amountToAdd)	{
	    		geld[i].amount+=(amountToAdd-geld[i].amount);
	    	}
	    }
	    warte(50,10);
	    
    }
    public static void ticketBestellung() {
    	//declaring variables
        Scanner tastatur = new Scanner(System.in);
        int c=0;
        int choice=0;
        double[] arr = {0,0};
        double zuZahlenderBetrag=0; 
        double rückgabebetrag=0;
        double anzahlFahrkarten=0;
        
        System.out.print("Fahrkartenbestellvorgang:\n");
        warte(75,25);
        System.out.print("\n\n\n");
        boolean repreat=true; 
        do {
 	       arr=fahrkartenbestellungErfassen();
 	       zuZahlenderBetrag+=arr[0];
 	       anzahlFahrkarten+=arr[1];
 	       System.out.println("\n\nBezahlen (1)\nWeitere Tickets kaufen (2)\n\nIhre Auswahl:");
 	       String choice_= tastatur.nextLine();
 	       warte(50,10);
 	       boolean choiceIsInt=isInteger(choice_);
 	       int x =1;
 	       while (x==1||choiceIsInt==false) {    
 	        	while (choiceIsInt==false) {
 		     	   System.out.println("\nFlasche Eingabe, Bitte nochmal versuchen.");
 		     	   System.out.println("Bezahlen (1)\nWeitere Tickets kaufen (2)\n\nIhre Auswahl:");
 		     	   choice_ = tastatur.nextLine();
 		     	   warte(50,10);
 		     	   choiceIsInt=isInteger(choice_);
 		        }
 	        	x=0;
 	        	choice = Integer.parseInt(choice_);
 		        if (choice >2||choice<1) {
 		        	
 		        	choiceIsInt=false;
 		        }
 		   }
 	       if (choice==1) {
 	    	   c=0;
 	       }else {
 	    	   c=1;
 	       }
        }while(c==1);
        rückgabebetrag= fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben(anzahlFahrkarten);
        rueckgeldAusgeben( rückgabebetrag, anzahlFahrkarten);
        // End of Purchase; Next one
        warte(250,8);	
     }
    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        if (length > 2) {
            return false;
        }
        int i = 0;
        
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
            	if(c!='.') {
                    return false;	
            	}
            }
        }
        return true;
    }
    public static boolean isDouble(String str) {
    	if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        if (length > 7) {
            return false;
        }
        int i = 0;
        
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
            	if(c!='.') {
                    return false;	
            	}
            }
        }
        return true;
    }
    public static void warte(int millisekunde, int length) {
    	for (int i = 0; i < length; i++)
	       {
	          System.out.print("=");
	          try {
	  			Thread.sleep(millisekunde);
	  		} catch (InterruptedException e) {
	  			e.printStackTrace();
	  		}
	       }
    	System.out.print("\n");
    }
    public static double[] fahrkartenbestellungErfassen() {
    	double[] ticketPreis= {0,2.90,3.30,3.6,1.9,8.6,9,9.6,23.5,24.3,24.9};
    	String[] ticketName= {null,"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
    	double[] arr= new double[2]; 
    	Scanner tastatur = new Scanner(System.in);
        String auswahl_; 
        int auswahl=0;
        int x=1;
        double zuZahlenderBetrag=0;
        String anzahlFahrkarten_;
        int anzahlFahrkarten=1;
        boolean auswahlIsInt;
        
        //Ticket Choice
        
        for (int i=1;i<ticketName.length;i++) {
        	System.out.println(ticketName[i]+" ("+i+")");
        }
        System.out.println("\nIhre Auswahl: ");
        auswahl_ = tastatur.nextLine();
        warte(25,10);
        auswahlIsInt=isInteger(auswahl_);
	    while (x==1||auswahlIsInt==false) {    
        	while (auswahlIsInt==false||auswahl_.charAt(0)=='0') {
	     	   System.out.println("\nFlasche Eingabe, Bitte nochmal versuchen.");
	     	   System.out.print("Wählen Sie eine Fahrkarte aus: \nEinzelfahrschein Berlin AB (1)\nEinzelfahrschein Berlin BC (2)\nEinzelfahrschein Berlin ABC (3)\nKurzstrecke (4)\nTageskarte Berlin AB (5)\nTageskarte Berlin BC (6)\nTageskarte Berlin ABC (7)\nKleingruppen-Tageskarte Berlin AB (8)\nKleingruppen-Tageskarte Berlin BC (9)\nKleingruppen-Tageskarte Berlin ABC (10)\nDein Auswahl: ");
	     	   auswahl_ = tastatur.nextLine();
	     	   warte(25,10);
	     	   auswahlIsInt=isInteger(auswahl_);
	        }
        	x=0;
	        auswahl = Integer.parseInt(auswahl_);
	        if (auswahl >10) {
	        	x=1;
	        	auswahlIsInt=false;
	        }
	    }
	    x=1;
        zuZahlenderBetrag = ticketPreis[auswahl];
        
        //Number Of Tickets Input
        System.out.println("\nAnzahl der fahrkarten: ");
        anzahlFahrkarten_ = tastatur.nextLine();
        warte(25,10);
        boolean anzahlIsInt=isInteger(anzahlFahrkarten_);
        while (anzahlIsInt==false||x==1) {
     	   while (anzahlIsInt==false||anzahlFahrkarten_.charAt(0)=='0') {
           System.out.println("Invalid Input");
     	   System.out.println("Min: 1, Max: 10");
     	   System.out.println("Anzahl der fahrkarten: ");
	        anzahlFahrkarten_ = tastatur.nextLine();
	        warte(25,10);
	        anzahlIsInt=isInteger(anzahlFahrkarten_);   
     	   }
        	x=0;
        	anzahlFahrkarten = Integer.parseInt(anzahlFahrkarten_);
	        if (anzahlFahrkarten >10) {
	        	x=1;
	        	anzahlIsInt=false;
	        }
	    }
        
        zuZahlenderBetrag = zuZahlenderBetrag*anzahlFahrkarten;
        arr[0]=zuZahlenderBetrag;
        arr[1]=anzahlFahrkarten;
        return arr;
    }
    
	public static double fahrkartenBezahlen(double zuZahlenderBetrag_){
		Scanner tastatur = new Scanner(System.in);
	       int x=1;
	       boolean inputIsRight;
	       double eingeworfeneMünze=0;
	       double eingezahlterGesamtbetrag_=0;
	       double rückgabebetrag_=0;
	       String eingeworfeneMünze_;
	       while(eingezahlterGesamtbetrag_ < zuZahlenderBetrag_)
	       {
	    	   x=1;
	    	   System.out.printf("Noch zu zahlen: %.2f\n" ,(zuZahlenderBetrag_-eingezahlterGesamtbetrag_));
	    	   int w = 5;
	    	   while (w!= 6) {
	    		   
	    		   System.out.print("Eingabe (mind. 5Ct, höchstens 50 Euro): ");
	    		   eingeworfeneMünze_ = tastatur.nextLine();
	    		   eingeworfeneMünze_ = eingeworfeneMünze_.replace(",", ".");
	    		   warte(50,10);
	    		   inputIsRight=isDouble(eingeworfeneMünze_);
	    		   while (x==1||inputIsRight==false) {    
	    	        	while (inputIsRight==false) {
	    	        	  System.out.print("Invalid Input\n");
	 	    	    	  System.out.print("Eingabe (mind. 0.05, höchstens 50 Euro): ");
	    		     	  eingeworfeneMünze_ = tastatur.nextLine();
	    		       	  eingeworfeneMünze_ = eingeworfeneMünze_.replace(",", ".");
	    		     	  warte(50,10);
	    		     	  inputIsRight=isDouble(eingeworfeneMünze_);
	    	        	}
	    	        	x=0;
	    	        	eingeworfeneMünze = Double.parseDouble(eingeworfeneMünze_);
	    		        if (eingeworfeneMünze >50||eingeworfeneMünze<0.05) {
	    		        	x=1;
	    		        	inputIsRight=false;
	    		        
	    	        	}
	    		        if (eingeworfeneMünze!=0.05&&eingeworfeneMünze!=0.1&&eingeworfeneMünze!=0.2&&eingeworfeneMünze!=0.5&&eingeworfeneMünze!=1&&eingeworfeneMünze!=2&&eingeworfeneMünze!=5&&eingeworfeneMünze!=10&&eingeworfeneMünze!=20&&eingeworfeneMünze!=50) {
	    		        	System.out.printf("\n%.0f ist kein richtige Geldbetrag\n",eingeworfeneMünze);
	    		        	x=1;
	    		        	inputIsRight=false;
	    		        }
	    		        if(eingeworfeneMünze==50) {
	    		        	geld[0].amount++;
	    		        }
	    		        if(eingeworfeneMünze==20) {
	    		        	geld[1].amount++;
	    		        }
	    		        if(eingeworfeneMünze==10) {
	    		        	geld[2].amount++;
	    		        }
	    		        if(eingeworfeneMünze==5) {
	    		        	geld[3].amount++;
	    		        }
	    		        if(eingeworfeneMünze==2) {
	    		        	geld[4].amount++;
	    		        }
	    		        if(eingeworfeneMünze==1) {
	    		        	geld[5].amount++;
	    		        }
	    		        if(eingeworfeneMünze==0.5) {
	    		        	geld[6].amount++;
	    		        }
	    		        if(eingeworfeneMünze==0.2) {
	    		        	geld[7].amount++;
	    		        }
	    		        if(eingeworfeneMünze==0.10) {
	    		        	geld[8].amount++;
	    		        }
	    		        if(eingeworfeneMünze==0.05) {
	    		        	geld[9].amount++;
	    		        }
	    		    }
	    	   
	    	   w++;
	    	   eingezahlterGesamtbetrag_ += eingeworfeneMünze;
	    	   }
	       }
	       rückgabebetrag_ = eingezahlterGesamtbetrag_ - zuZahlenderBetrag_ + 0.001;
	       return rückgabebetrag_;
	       }
		
	public static void fahrkartenAusgeben(double anzahlFahrkarten_){
		if (anzahlFahrkarten_==1) {
	    	   System.out.printf("\n%.0f Fahrschein wird ausgegeben\n",anzahlFahrkarten_);
	       }else {
	    	   System.out.printf("\n%.0f Fahrscheine werden ausgegeben\n",anzahlFahrkarten_);
	       }
	       
	       warte(250,8);
	}
	public static void schein20(int zeile) {
		if (zeile == 1) {
			System.out.printf( " * * * * * * * *");
		}
		if (zeile == 2) {
			System.out.printf( " *     20      *");
		}
		if (zeile == 3) {
			System.out.printf( " *    EURO     *");
		}
		if (zeile == 4) {
			System.out.printf( " *             *");
		}
		if (zeile == 5) {
			System.out.printf( " * * * * * * * *");
		}
		if (zeile == 6) {
			System.out.printf( "                ");
		}
	}
	public static void schein10(int zeile) {
		if (zeile ==1) {
			System.out.printf( " * * * * * * * *");
		}
		if (zeile ==2) {
			System.out.printf( " *     10      *");
		}
		if (zeile ==3) {
			System.out.printf( " *    EURO     *");
		}
		if (zeile ==4) {
			System.out.printf( " *             *");
		}
		if (zeile ==5) {
			System.out.printf( " * * * * * * * *");
		}
		if (zeile == 6) {
			System.out.printf( "                ");
		}
	}
	public static void schein5(int zeile) { 
		if (zeile ==1) {
			System.out.printf( " * * * * * * * *");
		}
		if (zeile ==2) {
			System.out.printf( " *     5       *");
		}
		if (zeile ==3) {
			System.out.printf( " *    EURO     *");
		}
		if (zeile ==4) {
			System.out.printf( " *             *");
		}
		if (zeile ==5) {
			System.out.printf( " * * * * * * * *");
		}
		if (zeile == 6) {
			System.out.printf( "                ");
		}
	}
	public static void münze2(int zeile) { 
		if (zeile ==1) {
			System.out.printf( "    *  *  *     ");
		}
		if (zeile ==2) {
			System.out.printf( "  *         *   ");
		}
		if (zeile ==3) {
			System.out.printf( " *     2     *  ");
		}
		if (zeile ==4) {
			System.out.printf( " *    EURO   *  ");
		}
		if (zeile ==5) {
			System.out.printf( "  *         *   ");
		}
		if (zeile == 6) {
			System.out.printf( "    *  *  *     ");
		}
	}
	public static void münze1(int zeile) { 
		if (zeile ==1) {
			System.out.printf( "    *  *  *     ");
		}
		if (zeile ==2) {
			System.out.printf( "  *         *   ");
		}
		if (zeile ==3) {
			System.out.printf( " *     1     *  ");
		}
		if (zeile ==4) {
			System.out.printf( " *    EURO   *  ");
		}
		if (zeile ==5) {
			System.out.printf( "  *         *   ");
		}
		if (zeile == 6) {
			System.out.printf( "    *  *  *     ");
		}
	}
	public static void münze05(int zeile) {  
		if (zeile ==1) {
			System.out.printf( "    *  *  *     ");
		}
		if (zeile ==2) {
			System.out.printf( "  *         *   ");
		}
		if (zeile ==3) {
			System.out.printf( " *     50    *  ");
		}
		if (zeile ==4) {
			System.out.printf( " *    CENT   *  ");
		}
		if (zeile ==5) {
			System.out.printf( "  *         *   ");
		}
		if (zeile == 6) {
			System.out.printf( "    *  *  *     ");
		}
	}
	public static void münze02(int zeile) { 
		if (zeile ==1) {
			System.out.printf( "    *  *  *     ");
		}
		if (zeile ==2) {
			System.out.printf( "  *         *   ");
		}
		if (zeile ==3) {
			System.out.printf( " *     20    *  ");
		}
		if (zeile ==4) {
			System.out.printf( " *    CENT   *  ");
		}
		if (zeile ==5) {
			System.out.printf( "  *         *   ");
		}
		if (zeile == 6) {
			System.out.printf( "    *  *  *     ");
		}
	}
	public static void münze01(int zeile) { 
		if (zeile ==1) {
			System.out.printf( "    *  *  *     ");
		}
		if (zeile ==2) {
			System.out.printf( "  *         *   ");
		}
		if (zeile ==3) {
			System.out.printf( " *     10    *  ");
		}
		if (zeile ==4) {
			System.out.printf( " *    CENT   *  ");
		}
		if (zeile ==5) {
			System.out.printf( "  *         *   ");
		}
		if (zeile == 6) {
			System.out.printf( "    *  *  *     ");
		}
	}
	public static void münze005(int zeile) { 
		if (zeile ==1) {
			System.out.printf( "    *  *  *     ");
		}
		if (zeile ==2) {
			System.out.printf( "  *         *   ");
		}
		if (zeile ==3) {
			System.out.printf( " *     5     *  ");
		}
		if (zeile ==4) {
			System.out.printf( " *    CENT   *  ");
		}
		if (zeile ==5) {
			System.out.printf( "  *         *   ");
		}
		if (zeile == 6) {
			System.out.printf( "    *  *  *     ");
		}
	}
	public static void rueckgeldAusgeben(double rückgabebetrag_,double anzahlFahrkarten_){
		int schein20A=0;
		int schein10A=0;
		int schein5A=0;
		int münze2A=0;
		int münze1A=0;
		int münze05A=0;
		int münze02A=0;
		int münze01A=0;
		int münze005A=0;
	       if(rückgabebetrag_ > 0.001)
	       {
	    	   System.out.printf("\nDer Rückgabebetrag in Höhe von %.2f EURO\n",rückgabebetrag_);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
	    	   int c=1;
	    	   while(rückgabebetrag_ >= 20.0) // 20 EURO-Schein
	           {
	    		   if(c==3||c==6||c==9) {
	    			   System.out.print("\n");
	    		   }
	    		   schein20A++;
		          rückgabebetrag_ -= 20.0;
		          geld[1].amount--;
		          c++;
	           }
	    	   while(rückgabebetrag_ >= 10.0) // 10 EURO-Schein
	           {
	    		   if(c==3||c==6||c==9) {
	    			   System.out.print("\n");
	    		   }
	    		   schein10A++;
		          rückgabebetrag_ -= 10.0;
		          geld[2].amount--;
		          c++;
	           }
	    	   while(rückgabebetrag_ >= 5.0) // 5 EURO-Schein
	           {
	    		   if(c==3||c==6||c==9) {
	    			   System.out.print("\n");
	    		   }
	    		   schein5A++;
	        	   
		          rückgabebetrag_ -= 5.0;
		          geld[3].amount--;
		          c++;
	           }
	           while(rückgabebetrag_ >= 2.0) // 2 EURO-Münzen
	           {
	        	   if(c==3||c==6||c==9) {
	    			   System.out.print("\n");
	    		   }
	        	   münze2A++;
		          rückgabebetrag_ -= 2.0;
		          geld[4].amount--;
		          c++;
	           }
	           while(rückgabebetrag_ >= 1.0) // 1 EURO-Münzen
	           {
	        	   if(c==3||c==6||c==9) {
	    			   System.out.print("\n");
	    		   }
	        	   münze1A++;
		          rückgabebetrag_ -= 1.0;
		          geld[5].amount--;
		          c++;
	           }
	           while(rückgabebetrag_ >= 0.5) // 50 CENT-Münzen
	           {
	        	   if(c==3||c==6||c==9) {
	    			   System.out.print("\n");
	    		   }
	        	   münze05A++;
		          rückgabebetrag_ -= 0.5;
		          geld[6].amount--;
		          c++;
	           }
	           while(rückgabebetrag_ >= 0.2) // 20 CENT-Münzen
	           {
	        	   if(c==3||c==6||c==9) {
	    			   System.out.print("\n");
	    		   }
	        	   münze02A++;
	 	          rückgabebetrag_ -= 0.2;
	 	         geld[7].amount--;
	 	         c++;
	           }
	           while(rückgabebetrag_ >= 0.1) // 10 CENT-Münzen
	           {
	        	   if(c==3||c==6||c==9) {
	    			   System.out.print("\n");
	    		   }
	        	   münze01A++;
		          rückgabebetrag_ -= 0.1;
		          geld[8].amount--;
		          c++;
	           }
	           while(rückgabebetrag_ >= 0.05)// 5 CENT-Münzen
	           {
	        	   if(c==3||c==6||c==9) {
	    			   System.out.print("\n");
	    		   }
	        	   münze005A++;
	 	          rückgabebetrag_ -= 0.05;
	 	          geld[9].amount--;
	 	          c++;
	           }
	       }
	       while (schein20A>0||schein10A>0||schein5A>0||münze2A>0||münze1A>0||münze05A>0||münze02A>0||münze01A>0||münze005A>0) {
		    	for (int i=1;i<7;i++)  {  
	    	   		int v=0;
	    	   		boolean check=true;
		    		if (schein20A>0&&check) {
		    	    	int copy=schein20A;
		    	    	while (copy>0&&v<3) {
			    	    	schein20(i);
			    	    	v++;
			    	    	copy--;
		    			}
		    	    	if (v==3) {
		    	    		check=false;
		    	    	}
		    	    	if (i==6) {
		    	    		schein20A=copy;
		    	    	}
		    	    }
					if (schein10A>0&&check) {
						int copy=schein10A;
						while (copy>0&&v<3) {
		    				schein10(i);
			    	    	v++;
			    	    	copy--;
		    			}
						if (v==3) {
		    	    		check=false;
		    	    	}
						if (i==6) {
							schein10A=copy;
		    	    	}
					}
					if (schein5A>0&&check) {
						int copy=schein5A;
						while (copy>0&&v<3) {
		    				schein5(i);
			    	    	v++;
			    	    	copy--;
		    			}
						if (v==3) {
		    	    		check=false;
		    	    	}
						if (i==6) {
							schein5A=copy;
		    	    	}
					}
					if (münze2A>0&&check) {
						int copy=münze2A;
						while (copy>0&&v<3) {
		    				münze2(i);
			    	    	v++;
			    	    	copy--;
		    			}
						if (v==3) {
		    	    		check=false;
		    	    	}
						if (i==6) {
							münze2A=copy;
		    	    	}
					}
					if (münze1A>0&&check) {
						int copy=münze1A;
						while (copy>0&&v<3) {
		    				münze1(i);
			    	    	v++;
			    	    	copy--;
		    			}
						if (v==3) {
		    	    		check=false;
		    	    	}
						if (i==6) {
							münze1A=copy;
		    	    	}
					}
					if (münze05A>0&&check) {
						int copy=münze05A;
						while (copy>0&&v<3) {
		    				münze05(i);
			    	    	v++;
			    	    	copy--;
		    			}
						if (v==3) {
		    	    		check=false;
		    	    	}
						if (i==6) {
							münze05A=copy;
		    	    	}
					}
					if (münze02A>0&&check) {
						int copy=münze02A;
						while (copy>0&&v<3) {
		    				münze02(i);
			    	    	v++;
			    	    	copy--;
		    			}
						if (v==3) {
		    	    		check=false;
		    	    	}
						if (i==6) {
							münze02A=copy;
		    	    	}
					}
					if (münze01A>0&&check) {
						int copy=münze01A;
						while (copy>0&&v<3) {
		    				münze01(i);
			    	    	v++;
			    	    	copy--;
		    			}
						if (v==3) {
		    	    		check=false;
		    	    	}
						if (i==6) {
							münze01A=copy;
		    	    	}
					}
					if (münze005A>0&&check) {
						int copy=münze005A;
						while (copy>0&&v<3) {
		    				münze005(i);
			    	    	v++;
			    	    	copy--;
		    			}
						if (v==3) {
		    	    		check=false;
		    	    	}
						if (i==6) {
							münze005A=copy;
		    	    	}
					}
					System.out.print("\n");
		    	}
	       }
	       if (anzahlFahrkarten_==1) {
	    	   System.out.println("\nVergessen Sie nicht, den Fahrscheine\n"+
	                   "vor Fahrtantritt entwerten zu lassen!\n"+
	                   "Wir wünschen Ihnen eine gute Fahrt.");
	       }else {
	    	   System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                   "vor Fahrtantritt entwerten zu lassen!\n"+
	                   "Wir wünschen Ihnen eine gute Fahrt.");
	       }

	       }
}
class geld{
	public String typ;
	public int amount;
	public geld(String typ_,int amount_) {
		typ=typ_;
		amount=amount_;
	}
}
