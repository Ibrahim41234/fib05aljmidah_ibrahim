package raumschiffe;

class Kapitan {
	private String name;
	private int startyear;
	public Kapitan(String name,int startyear) {
		this.name=name;
		this.startyear=startyear;
	}
	public String getName() {
		return name;
	}
	public int getStartYear() {
		return startyear;
	}
}
