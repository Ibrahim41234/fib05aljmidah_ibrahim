package raumschiffe;

class Ladung{
	public String bezichnung;
	public int menge;
	public Ladung(){
		
	}
	public Ladung(String bezeichnung, int menge){
		this.bezichnung=bezeichnung;
		this.menge=menge;
	}
	public void setBezeichnung(String name) {
		this.bezichnung=name;
	}
	public String getBezeichnung() {
		return this.bezichnung;
	}
	public void setMenge(int menge) {
		this.menge=menge;
	}
	public int getMenge() {
		return menge;
	}
}
