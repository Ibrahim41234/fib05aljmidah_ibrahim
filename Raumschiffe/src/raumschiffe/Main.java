package raumschiffe;

public class Main {
	public static void main(String[] args) {
		Raumschiff klingonen=new Raumschiff(1,100,100,100,100,"IKS Hegh'ta",2);
		klingonen.addLadung("ferengi Schneckensaft",200);
		klingonen.addLadung("Bat'leth Klingonen Schwert",200);
		Raumschiff romulaner=new Raumschiff(2,100,100,100,100,"IRW Khazara",2);
		romulaner.addLadung("Borg-Schrott",5);
		romulaner.addLadung("Rote Materie",2);
		romulaner.addLadung("Plasma-Waffe",50);
		Raumschiff vulkanier=new Raumschiff(0,80,80,50,100,"Ni'Var",5);
		vulkanier.addLadung("Forschungssonde",35);
		vulkanier.addLadung("Photonentorpedo",3);
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		vulkanier.reparaturDurchfuehren(true, true, true, 5);
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		romulaner.zustandRaumschiff();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		Kapitan thamer =new Kapitan("thamer",1999);
}
}
