package raumschiffe;
import java.util.ArrayList;



class Raumschiff{
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	ArrayList<String> broadcastKommunikator= new ArrayList<String>();
	ArrayList<Ladung> ladungsverzeichnis= new ArrayList<Ladung>();
	public Raumschiff(){
		
	}
	public Raumschiff(int photonentorpedoAnzahl,int energieversorgungInProzent,int zustandSchildeInProzent,int zustandHuelleInProzent,int zustandLebenserhaltungssystemeInProzent,String schiffsname,int anzahlDroiden){
		this.photonentorpedoAnzahl=photonentorpedoAnzahl;
		this.energieversorgungInProzent=energieversorgungInProzent;
		this.schildeInProzent=zustandSchildeInProzent;
		this.huelleInProzent=zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent=zustandLebenserhaltungssystemeInProzent;
		this.androidenAnzahl=anzahlDroiden;
		this.schiffsname=schiffsname;
	}
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl=photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzent) {
		this.energieversorgungInProzent=zustandEnergieversorgungInProzent;
	}
	public int getSchildeInProzent () {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildInProzent) {
		this.schildeInProzent=schildInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent=huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent=lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl= androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname=schiffsname;
	}
	public void addLadung (String bezeivhnung,int menge) {
		Ladung ladung=new Ladung(bezeivhnung,menge);
		ladungsverzeichnis.add(ladung);
	}
	private void treffer (Raumschiff r) {
		System.out.println(r.schiffsname+" wurde getroffen!");
		if(r.schildeInProzent<1) {
			r.setEnergieversorgungInProzent(r.energieversorgungInProzent-=50);
			r.setHuelleInProzent(r.huelleInProzent-=50);
		}
		if (r.schildeInProzent>0) {
			r.setSchildeInProzent(r.schildeInProzent-=50);
		}
		
		if(r.huelleInProzent<1) {
			r.setLebenserhaltungssystemeInProzent(0);
			r.nachrichtAnAlle("Die Lebenserhaltungssysteme Wurde vernichtet");
		}
		
		
	}
	public void photonentorpedoSchiessen (Raumschiff r) {
		if (photonentorpedoAnzahl>0) {
			System.out.println("Photonentorpedo abgeschossen");
			photonentorpedoAnzahl-=1;
			treffer(r);
		}
		else {
			System.out.println("Keine Photonentorpedos gefunden!");
			System.out.println("-=*Click*=-");
		}
	}
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (energieversorgungInProzent<50) {
			System.out.println("-=*Click*=-");
		}else {
			energieversorgungInProzent-=50;
			System.out.println("Phaserkanone abgeschossen");
			treffer(r);
		}
	}
	public void nachrichtAnAlle (String nachricht) {
		broadcastKommunikator.add(nachricht);
		System.out.println(nachricht);
		
	}
	public void eintraegeLogbuchZurueckgeben(){
		for (int i=0;i<broadcastKommunikator.size();i++) {
			System.out.println(broadcastKommunikator.get(i));
		}
	}
	public void photonentorpedosLaden(int anzahlTorpedos) {
		boolean torpedosExist=false;
		for(int i=0;i<ladungsverzeichnis.size();i++) {
			if(ladungsverzeichnis.get(i).bezichnung=="Photonentorpedo") {
				torpedosExist=true;
				if (ladungsverzeichnis.get(i).menge<anzahlTorpedos) {
					anzahlTorpedos=ladungsverzeichnis.get(i).menge;
				}
				ladungsverzeichnis.get(i).menge-=anzahlTorpedos;
				photonentorpedoAnzahl+=anzahlTorpedos;
			}
		}
		if(torpedosExist=false) {
			System.out.println("Kein Torpedos mehr im Lager vorhanden");
		}

		
		
	}
	public void reparaturDurchfuehren (boolean schutzschilde,boolean energieversirgung,boolean schiffsheulle, int anzahlDroiden) {
		if(androidenAnzahl>0) {
			int anzahltruestrukturen=0;
			if (schutzschilde==true) {
				anzahltruestrukturen++;
			}
			if (energieversirgung==true) {
				anzahltruestrukturen++;
			}
			if (schiffsheulle==true) {
				anzahltruestrukturen++;
			}
			int randomNum = (int)(Math.random() * 101);
			int androiden=anzahlDroiden;
			if (androiden>androidenAnzahl) {
				androiden=androidenAnzahl;
			}
			
			int reparaturZahl=randomNum*androiden/anzahltruestrukturen;
			if (schutzschilde==true) {
				schildeInProzent+=reparaturZahl;
				if(schildeInProzent>100) {
					schildeInProzent=100;
				}
			}
			if (energieversirgung==true) {
				energieversorgungInProzent+=reparaturZahl;
				if(energieversorgungInProzent>100) {
					energieversorgungInProzent=100;
				}
			}
			if (schiffsheulle==true) {
				huelleInProzent+=reparaturZahl;
				if(huelleInProzent>100) {
					huelleInProzent=100;
				}
			}
		}
		else {
			System.out.println("keine Androiden mehr vorhanden");
			broadcastKommunikator.add("keine Androiden mehr vorhanden");
		}
	}
	public void zustandRaumschiff() {
		System.out.println("Schiffsname = "+schiffsname);
		System.out.println("Photonentorpedo Anzahl = "+photonentorpedoAnzahl);
		System.out.println("Energieversorgung In Prozent = "+energieversorgungInProzent);
		System.out.println("Schilde In Prozent = "+schildeInProzent);
		System.out.println("Huelle In Prozent = "+huelleInProzent);
		System.out.println("Lebenserhaltungssysteme In Prozent = "+lebenserhaltungssystemeInProzent);
		System.out.println("androidenAnzahl =" + androidenAnzahl);
	}
	public void ladungsverzeichnisAusgeben() {
		for (int i=0;i<ladungsverzeichnis.size();i++) {
			System.out.println(ladungsverzeichnis.get(i).bezichnung+": "+ladungsverzeichnis.get(i).menge);
		}
	}
	public void ladungsverzeichnisAufraeumen() {
		for (int i=0;i<ladungsverzeichnis.size();i++) {
			if (ladungsverzeichnis.get(i).menge<1) {
				ladungsverzeichnis.remove(i);
			}
		}
	}
	
}
